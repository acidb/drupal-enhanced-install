; Make file for Drupal Installation
; Last Changes: 2014-09-30

; Core version
; ------------

core = 7.x

; API version
; ------------

api = 2

; Core project
; ------------

projects[drupal][version] = 7.31



; Profile
; -------

projects[enhanced][type] = "profile"
projects[enhanced][download][type] = "git"
projects[enhanced][download][destination] = "profiles"
projects[enhanced][download][url]= "https://acidb@bitbucket.org/acidb/enhanced.git"
