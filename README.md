Drupal Enhanced Installer
=========================

Just download this make file and run

    drush make drupal-enhanced.make

This should install Drupal with Enhanced Profile and the
Cloned Sheep Template.

For me a good starting point for a Drupal website with some useful modules.
